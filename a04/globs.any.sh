#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.any.sh - displays entries in the current directory that match any of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ANY of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched any glob
		1 if no name matched any glob
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*' '*z*'
		123
		abc
		abc123

	EOF
	exit 2
} >&2 && {
	>&2 && {
	list=$(ls)
		mylist=""
	for arg in $@; do

		
			
			bool=false
			for match in $list; do
			
				case $arg in
					$match) 
						mylist=$mylist"\n"$arg
						;;
				esac
				
			done
			
	
	
		
	done
	mylist=$(echo $mylist | sort | uniq )
	
	
	
}

