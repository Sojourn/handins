#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.all.sh - displays entries in the current directory that match all of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ALL of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched all globs
		1 if no name matched all globs
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*'
		abc123

	EOF
	exit 2
} >&2 && {
	echo $#
	list=$(ls)
		mylist=""
	for arg in $@; do
		echo $arg
		
			
			
			for match in $list; do
			
				case $arg in
					$match) 
						mylist=$mylist"\n"$arg
						;;
				esac
				
			done
			
	
	
		
	done
	if [ $# -eq 1 ]; then 
		mylist=$(echo $mylist | sort | uniq )
	else
		mylist=$(echo $mylist | sort | uniq -d)
	fi
	echo $mylist
	
	
	
}
